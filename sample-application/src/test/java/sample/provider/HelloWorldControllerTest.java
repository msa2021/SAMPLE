package sample.provider;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import sample.config.DisabledSecurityConfiguration;
import sample.service.DbService;

@WebMvcTest({ HelloWorldController.class, DisabledSecurityConfiguration.class })
class HelloWorldControllerTest {

  @MockBean
  private DbService db;

  @Autowired
  private MockMvc mvc;

  @Test
  void testHelloNameWithDb() throws Exception {

    BDDMockito.given(db.collectionNames()).willReturn(Collections.singleton("mycoll"));
    BDDMockito.given(db.countInCollection("mycoll")).willReturn(3l);

    mvc.perform(get("/api/v1/hello/{name}", "John").contentType("application/json")).andExpect(status().isOk())
        .andExpect(content().json("{\"message\":\"Hello John\"}"));
  }

}
