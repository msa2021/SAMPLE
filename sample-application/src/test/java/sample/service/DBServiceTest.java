package sample.service;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.client.MongoCollection;

@SpringBootTest(classes = { DbService.class })
class DBServiceTest {

  @Autowired
  private DbService db;

  @MockBean
  private MongoTemplate mongoTemplate;

  @Mock
  MongoCollection<Document> collection;

  Document insertedDocument;

  @BeforeEach
  void setUp() throws Exception {

    BDDMockito.given(mongoTemplate.getCollection("collection1")).willReturn(collection);
    BDDMockito.given(collection.countDocuments()).willReturn(30L);
    BDDMockito.given(mongoTemplate.getCollectionNames())
        .willReturn(Stream.of("collection1", "collection2", "collection3").collect(Collectors.toSet()));

    insertedDocument = new Document();
    BDDMockito.given(mongoTemplate.insert(BDDMockito.any(Document.class), BDDMockito.anyString()))
        .willReturn(insertedDocument);
  }

  @Test
  void testCountInCollection() throws Exception {
    Assertions.assertThat(db.countInCollection("collection1")).isEqualTo(30);
  }

  @Test
  void testCollectionNames() throws Exception {
    Assertions.assertThat(db.collectionNames()).containsExactly("collection1", "collection2", "collection3");
  }

  @Test
  void insertDocument() throws Exception {
    Assertions.assertThat(db.insertDocument("collection1", new Document("key1", "value1"))).isEqualTo(insertedDocument);
  }

}
