package sample.service;

import java.util.List;
import java.util.Set;

import org.bson.Document;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class DbService {

  private MongoTemplate mongoTemplate;

  public DbService(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @NewSpan
  public long countInCollection(String collectionName) {
    return mongoTemplate.getCollection(collectionName).countDocuments();
  }

  @NewSpan
  public Set<String> collectionNames() {
    return mongoTemplate.getCollectionNames();
  }

  @NewSpan
  public List<Document> allDocuments(String collectionName) {
    return mongoTemplate.findAll(Document.class, collectionName);
  }

  @NewSpan
  public Document insertDocument(String collectionName, Document document) {
    return mongoTemplate.insert(document, collectionName);
  }

  @NewSpan
  public Document loadDocument(String collectionName, String id) {
    return mongoTemplate.getCollection(collectionName).find(new Document("id", id)).first();
  }

}
