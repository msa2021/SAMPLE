package sample.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.Lifecycle;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.fasterxml.jackson.databind.JsonNode;

import de.knowis.cp.common.environment.featureflag.condition.ConditionalOnFeatureFlag;
import de.knowis.cp.common.kafka.autoconfiguration.KafkaTopicsHelper;
import de.knowis.cp.util.contract.Contracts;

@Service
@ConditionalOnFeatureFlag(de.knowis.cp.common.kafka.autoconfiguration.KafkaConfiguration.FEATURE_NAME)
public class EventService {

  private static final Logger log = LoggerFactory.getLogger(EventService.class);

  private KafkaTemplate<String, JsonNode> kafkaTemplate;
  private EventListenerImpl listener;
  private KafkaTopicsHelper topicsHelper;

  public EventService(KafkaTemplate<String, JsonNode> kafkaTemplate, EventListenerImpl listener,
      KafkaTopicsHelper topicsHelper) {
    this.kafkaTemplate = kafkaTemplate;
    this.listener = listener;
    this.topicsHelper = topicsHelper;
  }

  public List<TopicName> getTopicNames() {
    return Arrays.asList(new TopicName(topicsHelper.getFullyQualifiedTopicName("myEventTopic")));

  }

  public List<JsonNode> getEvents() {
    return Stream.of(listener.getLastEvent()).collect(Collectors.toList());
  }

  public JsonNode sendEvent(String topic, JsonNode payload)
      throws InterruptedException, ExecutionException, TimeoutException {
    log.debug("send event to topic {}", topic);

    String fullyQualifiedTopicName = topicsHelper.getFullyQualifiedTopicName(topic);
    ListenableFuture<SendResult<String, JsonNode>> future = kafkaTemplate.send(fullyQualifiedTopicName, payload);

    SendResult<String, JsonNode> sendResult = future.get(10, TimeUnit.SECONDS);

    return sendResult.getProducerRecord().value();
  }

  class TopicName {
    private String name;

    public TopicName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

  }

  public static class EventListenerImpl
      implements AcknowledgingMessageListener<String, JsonNode>, ErrorHandler, Lifecycle {

    private static final Logger log = LoggerFactory.getLogger(EventService.EventListenerImpl.class);

    private JsonNode lastEvent;
    private ConcurrentMessageListenerContainer<String, JsonNode> container;

    public JsonNode getLastEvent() {
      return lastEvent;
    }

    @Override
    public void handle(Exception thrownException, ConsumerRecord<?, ?> data) {
      log.error("exception while receiving event {}", data, thrownException);
    }

    @Override
    public void onMessage(ConsumerRecord<String, JsonNode> data, Acknowledgment acknowledgment) {
      log.info("received payload='{}'", data);
      lastEvent = data.value();
      acknowledgment.acknowledge();
    }

    public void setContainer(ConcurrentMessageListenerContainer<String, JsonNode> container) {
      Contracts.notNull(container, "container");
      this.container = container;
    }

    @Override
    public void start() {
      if (container == null) {
        throw new IllegalStateException("container must be set");
      }
      container.start();
    }

    @Override
    public void stop() {
      if (container == null) {
        throw new IllegalStateException("container must be set");
      }
      container.stop();
    }

    @Override
    public boolean isRunning() {
      if (container == null) {
        return false;
      }
      return container.isRunning();
    }

  }

}
