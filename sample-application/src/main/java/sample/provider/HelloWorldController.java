package sample.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sample.service.DbService;

@RestController
@RequestMapping(value = "/api/v1/hello", produces = MediaType.APPLICATION_JSON_VALUE)
public class HelloWorldController {

  private static final Logger log = LoggerFactory.getLogger(HelloWorldController.class);

  private DbService db;

  public HelloWorldController (DbService db) {
    this.db = db;
  }

  @GetMapping("/{name}")
  public HelloWorldMessage helloNameWithDb(@PathVariable("name") String name) {
    log.debug("Hello Service {}", name);
    if (name != null) {
      db.collectionNames().forEach(log::info);
      log.info("collection {} contains {} documents", name, db.countInCollection(name));
    }
    return new HelloWorldMessage("Hello " + name);
  }

  @GetMapping
  public HelloWorldMessage home(@AuthenticationPrincipal Jwt jwt) {
    log.debug("Hello home {}", jwt);

    final String message;
    if (jwt == null) {
      message = "anonymous";
    } else {
      message = String.format("Hello %s (got from %s)", jwt.getClaimAsString("name"), jwt.getIssuer());
    }

    return new HelloWorldMessage(message);
  }

}
