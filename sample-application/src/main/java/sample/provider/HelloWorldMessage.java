package sample.provider;

public class HelloWorldMessage {

  private String message;

  public HelloWorldMessage(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  @Override
  public String toString() {
    return "HelloWorldMessage [message=" + message + "]";
  }

}
