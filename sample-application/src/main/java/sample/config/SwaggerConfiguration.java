package sample.config;

import org.apache.commons.lang3.ArrayUtils;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.GroupedOpenApi.Builder;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.knowis.cp.common.openapi.security.SecurityCustomizer;
import de.knowis.cp.common.openapi.ui.EnableOpenApiUi;
import de.knowis.cp.common.security.autoconfiguration.OidcConfiguration;
import de.knowis.cp.common.solution.autoconfiguration.SolutionConfigurationProperties;
import io.swagger.v3.oas.models.info.Info;

@EnableOpenApiUi
@Configuration
public class SwaggerConfiguration {

  private final SolutionConfigurationProperties solutionConfiguration;
  private final OidcConfiguration oidcConfig;

  public SwaggerConfiguration(SolutionConfigurationProperties solutionConfiguration, OidcConfiguration oidcConfig) {
    this.solutionConfiguration = solutionConfiguration;
    this.oidcConfig = oidcConfig;
  }

  private static final String ACRONYM_GROUP_TITLE = "sample";

  private static final String ACRONYM_GROUP_DESC = "sample service";

  private static final String ACRONYM_GROUP_VERSION = "1.0.0";

  private static final String[] ACRONYM_GROUP_PACKAGES_TO_SCAN = new String[] { "sample.provider" };

  private static final String[] ACRONYM_GROUP_PACKAGES_TO_EXCLUDE = new String[0];

  @Bean
  public OpenApiCustomiser securityCustomizer() {
    return new SecurityCustomizer(oidcConfig::getUserTokenUrl);
  }

  @Bean
  public GroupedOpenApi getGroupedOpenApi() {
    Builder builder = GroupedOpenApi.builder().group(solutionConfiguration.getAcronym())
        .addOpenApiCustomiser(api -> api.info(new Info().title(ACRONYM_GROUP_TITLE).description(ACRONYM_GROUP_DESC).version(ACRONYM_GROUP_VERSION)))
        .addOpenApiCustomiser(securityCustomizer());


    if (ArrayUtils.isNotEmpty(ACRONYM_GROUP_PACKAGES_TO_SCAN)) {
      builder.packagesToScan(ACRONYM_GROUP_PACKAGES_TO_SCAN);
    }
    if (ArrayUtils.isNotEmpty(ACRONYM_GROUP_PACKAGES_TO_EXCLUDE)) {
      builder.packagesToExclude(ACRONYM_GROUP_PACKAGES_TO_EXCLUDE);
    }

    return builder.build();
  }
}
