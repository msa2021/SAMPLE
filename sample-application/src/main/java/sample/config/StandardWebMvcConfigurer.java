package sample.config;

import static de.knowis.cp.util.contract.Contracts.notNull;
import static org.springframework.http.CacheControl.noCache;

import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

public class StandardWebMvcConfigurer implements WebMvcConfigurer {

  private boolean enableRedirect;

  public StandardWebMvcConfigurer(boolean enableRedirect) {
    this.enableRedirect = enableRedirect;
  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    if (enableRedirect) {
      registry.addViewController("/").setViewName("redirect:/swagger-ui.html");
    }
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    notNull(registry, "registry");
    registry.addInterceptor(buildDisableCacheInterceptor());
  }

  private WebContentInterceptor buildDisableCacheInterceptor() {
    // General cache disabling. If we want to have more control, we should use or
    // implement something like https:github.comfoo4uspring-mvc-cache-control
    WebContentInterceptor result = new WebContentInterceptor();
    result.setCacheControl(noCache());
    result.setCacheSeconds(0);
    result.setVaryByRequestHeaders("*");
    return result;
  }
}
