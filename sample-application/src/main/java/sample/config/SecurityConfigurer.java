package sample.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import de.knowis.cp.common.security.autoconfiguration.HttpSecurityConfigurer;

public class SecurityConfigurer implements HttpSecurityConfigurer {

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        // enable unauthenticated access to api documentation resources
        .antMatchers("/api-docs/*").permitAll()
        // enable unauthenticated access to actuator status and health
        .antMatchers("/actuator/health", "/actuator/info").permitAll() //

        // force authentication for all other resources
        .anyRequest().authenticated();
  }

}
