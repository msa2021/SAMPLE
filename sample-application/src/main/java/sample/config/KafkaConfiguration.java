package sample.config;

import java.util.Map;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.ContainerProperties.AckMode;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.fasterxml.jackson.databind.JsonNode;

import de.knowis.cp.common.environment.featureflag.condition.ConditionalOnFeatureFlag;
import de.knowis.cp.common.kafka.autoconfiguration.KafkaConfiguration.KafkaConsumerConfiguration;
import de.knowis.cp.common.kafka.autoconfiguration.KafkaConfiguration.KafkaProducerConfiguration;
import de.knowis.cp.common.kafka.autoconfiguration.KafkaTopicsHelper;
import de.knowis.cp.util.text.TextFormatter;
import sample.service.EventService.EventListenerImpl;

@Configuration
@EnableKafka
@ConditionalOnFeatureFlag("feature.kafka-events.enabled")
public class KafkaConfiguration {

  private static final Logger log = LoggerFactory.getLogger(KafkaConfiguration.class);

  @Bean
  @Autowired
  public KafkaTemplate<String, JsonNode> kafkaTemplateGenericEvents(
      KafkaProducerConfiguration kafkaProducerConfiguration) {
    log.debug("Create KafkaTemplate()...");
    DefaultKafkaProducerFactory<String, JsonNode> producerFactory = new DefaultKafkaProducerFactory<>(
        kafkaProducerConfiguration);
    return new KafkaTemplate<>(producerFactory);
  }

  @Bean
  @Autowired
  public ConsumerFactory<String, JsonNode> consumerFactory(KafkaConsumerConfiguration kafkaConsumerConfiguration) {
    log.debug("Create ConsumerFactory...");
    Map<String, Object> consumerConfigs = kafkaConsumerConfiguration;
    consumerConfigs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, Boolean.FALSE);
    return new DefaultKafkaConsumerFactory<>(consumerConfigs, null, new JsonDeserializer<>(JsonNode.class));
  }

  @Bean
  public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, JsonNode>> kafkaListenerContainerFactory(
      ConsumerFactory<String, JsonNode> consumerFactory) {
    ConcurrentKafkaListenerContainerFactory<String, JsonNode> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory);

    return factory;
  }

  @Bean
  @Autowired
  public EventListenerImpl sampleListener(KafkaTopicsHelper topicsHelper,
      ConsumerFactory<String, JsonNode> consumerFactory) {

    String fullyQualifiedTopicName = topicsHelper.getFullyQualifiedTopicName("myEventTopic");
    String groupId = "sample-application";
    ContainerProperties containerProperties = new ContainerProperties(fullyQualifiedTopicName);
    containerProperties.setGroupId(groupId);
    String clientId = TextFormatter.format("{}_{}", groupId, UUID.randomUUID().toString());
    containerProperties.setClientId(clientId);
    containerProperties.setAckMode(AckMode.MANUAL);

    EventListenerImpl listener = new EventListenerImpl();
    containerProperties.setMessageListener(listener);

    ConcurrentMessageListenerContainer<String, JsonNode> container = new ConcurrentMessageListenerContainer<>(
        consumerFactory, containerProperties);
    container.setConcurrency(1);

    listener.setContainer(container);

//    listener.start();
    return listener;
  }

}
