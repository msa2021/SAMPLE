package sample.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ConditionalOnProperty(prefix = "feature.webmvc", value = "enabled", havingValue = "true", matchIfMissing = true)
public class StandardWebMvcAutoConfiguration {

  @Profile("!local")
  @Bean
  public WebMvcConfigurer standardWebMvcConfigurer() {
    return new StandardWebMvcConfigurer(false);
  }

  @Profile("local")
  @Bean
  public WebMvcConfigurer standardWebMvcConfigurerLocal() {
    return new StandardWebMvcConfigurer(true);
  }

}
